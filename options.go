package unisender

import "net/http"

type Options struct {
	apiKey string
	apiURL string
	httpCl *http.Client
}

type Option func(*Options)

func APIKey(val string) Option {
	return func(opts *Options) {
		opts.apiKey = val
	}
}

func APIUrl(val string) Option {
	return func(opts *Options) {
		opts.apiURL = val
	}
}

func HttpClient(val *http.Client) Option {
	return func(opts *Options) {
		opts.httpCl = val
	}
}

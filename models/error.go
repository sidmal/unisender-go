package models

import "fmt"

type Error struct {
	Status  string `json:"status"`
	Code    int    `json:"code"`
	Message string `json:"message"`
}

func (m Error) Error() string {
	return fmt.Sprintf("%s: code: %d, message: %s", m.Status, m.Code, m.Message)
}

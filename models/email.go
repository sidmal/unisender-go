package models

type EmailSendRequest struct {
	Message EmailSendMessage `json:"message"`
}

type EmailSendResponse struct {
	Status       string            `json:"status"`
	JobId        string            `json:"job_id"`
	Emails       []string          `json:"emails"`
	FailedEmails map[string]string `json:"failed_emails"`
}

type EmailSendMessage struct {
	Recipients          []Recipients      `json:"recipients"`
	TemplateId          string            `json:"template_id,omitempty"`
	Tags                []string          `json:"tags,omitempty"`
	SkipUnsubscribe     int               `json:"skip_unsubscribe,omitempty"`
	GlobalLanguage      string            `json:"global_language,omitempty"`
	TemplateEngine      string            `json:"template_engine,omitempty"`
	GlobalSubstitutions map[string]string `json:"global_substitutions,omitempty"`
	GlobalMetadata      map[string]string `json:"global_metadata,omitempty"`
	Body                *Body             `json:"body,omitempty"`
	Html                string            `json:"html,omitempty"`
	Plaintext           string            `json:"plaintext,omitempty"`
	AMP                 string            `json:"amp,omitempty"`
	Subject             string            `json:"subject"`
	FromEmail           string            `json:"from_email,omitempty"`
	FromName            string            `json:"from_name,omitempty"`
	ReplyTo             string            `json:"reply_to,omitempty"`
	TrackLinks          int               `json:"track_links,omitempty"`
	TrackRead           int               `json:"track_read,omitempty"`
	BypassGlobal        int               `json:"bypass_global,omitempty"`
	BypassUnavailable   int               `json:"bypass_unavailable,omitempty"`
	BypassUnsubscribed  int               `json:"bypass_unsubscribed,omitempty"`
	BypassComplained    int               `json:"bypass_complained,omitempty"`
	Headers             map[string]string `json:"headers,omitempty"`
	Attachments         []Attachment      `json:"attachments,omitempty"`
	InlineAttachments   []Attachment      `json:"inline_attachments,omitempty"`
	Options             *Options          `json:"options,omitempty"`
}

type Recipients struct {
	Email         string                 `json:"email"`
	Substitutions map[string]interface{} `json:"substitutions,omitempty"`
	Metadata      map[string]string      `json:"metadata,omitempty"`
}

type Attachment struct {
	Type    string `json:"type"`
	Name    string `json:"name"`
	Content string `json:"content"`
}

type Options struct {
	SendAt          string `json:"send_at"`
	UnsubscribeUrl  string `json:"unsubscribe_url"`
	CustomBackendId int    `json:"custom_backend_id"`
	SmtpPoolId      string `json:"smtp_pool_id"`
}

type Body struct {
	Html      string `json:"html"`
	Plaintext string `json:"plaintext"`
	Amp       string `json:"amp"`
}

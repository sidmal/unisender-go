package unisender

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"net/url"
	"time"

	"gitlab.com/sidmal/unisender-go/models"
)

var (
	ErrApiKeyRequired          = errors.New("api key option required")
	ErrApiURLIncorrect         = errors.New("incorrect api url is set")
	ErrResponseUndefinedFormat = errors.New("received response has undefined format")
)

const (
	defaultHttpRequestTimeout = 3
	defaultUnisenderGoAPIURL  = "https://go1.unisender.ru"
)

const (
	pathEmailSend = "/ru/transactional/api/v1/email/send.json"
)

type GoUnisender struct {
	apiKey string
	apiURL *url.URL
	httpCl *http.Client
}

func NewUnisenderGo(opts ...Option) (*GoUnisender, error) {
	options := &Options{}
	for _, opt := range opts {
		opt(options)
	}

	if options.apiKey == "" {
		return nil, ErrApiKeyRequired
	}

	if options.apiURL == "" {
		options.apiURL = defaultUnisenderGoAPIURL
	}

	u, err := url.Parse(options.apiURL)
	if err != nil {
		return nil, ErrApiURLIncorrect
	}

	res := &GoUnisender{
		apiKey: options.apiKey,
		apiURL: u,
		httpCl: options.httpCl,
	}

	if res.httpCl == nil {
		res.httpCl = &http.Client{
			Timeout: defaultHttpRequestTimeout * time.Second,
		}
	}

	return res, nil
}

func (m GoUnisender) EmailSend(ctx context.Context, req models.EmailSendRequest) (*models.EmailSendResponse, error) {
	reqBytes, err := json.Marshal(req)
	if err != nil {
		return nil, err
	}

	res, err := m.request(ctx, pathEmailSend, http.MethodPost, bytes.NewReader(reqBytes), new(models.EmailSendResponse))
	if err != nil {
		return nil, err
	}

	resTyped, ok := res.(*models.EmailSendResponse)
	if !ok {
		return nil, ErrResponseUndefinedFormat
	}

	return resTyped, nil
}

func (m GoUnisender) request(
	ctx context.Context,
	path, method string,
	data io.Reader,
	receiver interface{},
) (interface{}, error) {
	m.apiURL.Path = path
	req, err := http.NewRequestWithContext(ctx, method, m.apiURL.String(), data)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("X-API-KEY", m.apiKey)

	rsp, err := m.httpCl.Do(req)
	if err != nil {
		return nil, err
	}

	rspBody, err := io.ReadAll(rsp.Body)
	defer func() {
		_ = rsp.Body.Close()
	}()
	if err != nil {
		return nil, err
	}

	if rsp.StatusCode != http.StatusOK {
		return nil, m.parseApiError(rspBody)
	}

	err = json.Unmarshal(rspBody, &receiver)

	return receiver, err
}

func (m GoUnisender) parseApiError(rsp []byte) error {
	res := models.Error{}
	err := json.Unmarshal(rsp, &res)
	if err != nil {
		return err
	}

	return res
}

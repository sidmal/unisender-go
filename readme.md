Unisender Go golang SDK
=============

## Installation

`go get gitlab.com/sidmal/unisender-go`

## Usage

```go
package main

import (
	"context"
	"log"

	"gitlab.com/sidmal/unisender-go"
	"gitlab.com/sidmal/unisender-go/models"
)

func main() {
	opts := []unisender.Option{
		unisender.APIKey("***********"),
	}
	unisndr, err := unisender.NewUnisenderGo(opts...)
	if err != nil {
		log.Fatal(err)
	}

	emailReq := models.EmailSendRequest{
		Message: models.EmailSendMessage{
			Recipients: []models.Recipients{
				{
					Email: "test@gmail.com",
					Substitutions: map[string]interface{}{
						"param1": "value1",
						"param2": "value2",
						"param3": "value3",
					},
				},
			},
			TemplateId:     "9a14d640-e429-4995-9f5f-bc40767afef5",
			GlobalLanguage: "en",
			Subject:        "Email subject",
		},
	}
	_, err = unisndr.EmailSend(context.Background(), emailReq)
	if err != nil {
		log.Fatalf("request to unisender failed with error %v", err)
	}

	log.Println("success")
}
```